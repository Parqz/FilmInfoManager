package com.parqz.filminfomanager;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Random;

public class AddCommentActivity extends AppCompatActivity {
    int expId1,expId2,expId3;
    Random random = new Random();
    String cmnt=null;
    Account account;
    Film film;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_comment);

        Intent parentIntent = getIntent();
        Long acId = parentIntent.getLongExtra(FilmSpecActivity.ACCOUNT_ID,1);
        Long FId = parentIntent.getLongExtra(FilmSpecActivity.FILM_ID,1);
        account = Account.findById(Account.class,acId);
        film = Film.findById(Film.class,FId);
        TextView headerTV = (TextView) findViewById(R.id.add_comment_header);
        headerTV.setText("Add Comment For Film: "+film.getName());

    }
    public void addComment(View view)
    {

        EditText cmEdittext = (EditText) findViewById(R.id.comment_editText);
        cmnt = cmEdittext.getText().toString();
        List<TComment> tcms = TComment.listAll(TComment.class);
        TComment tComment = new TComment(cmnt,film,account);
        tComment.save();
        expId1= random.nextInt(3)+1;
        Expert exp1 = Expert.findById(Expert.class,2);
        expId2= random.nextInt(3)+1;
        Expert exp2 = Expert.findById(Expert.class,3);
        expId3= random.nextInt(3)+1;
        Expert exp3 = Expert.findById(Expert.class,4);
        Cmntexpt cmex1 = new Cmntexpt(cmnt,-1,exp1);
        cmex1.save();
        Cmntexpt cmex2 = new Cmntexpt(cmnt,-1,exp2);
        cmex2.save();
        Cmntexpt cmex3 = new Cmntexpt(cmnt,-1,exp3);
        cmex3.save();
        Toast.makeText(this,"Comment Sent For Checking..."  , Toast.LENGTH_SHORT).show();

    }
}
