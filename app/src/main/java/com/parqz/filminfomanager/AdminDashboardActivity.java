package com.parqz.filminfomanager;

import android.content.Intent;
import android.content.res.AssetManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class AdminDashboardActivity extends AppCompatActivity {

    public static final String ACCOUNT_ID = "com.parqz.filminfomanager.AdminDashboardActivity_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_dashboard);
    }
    public void searchFilm(View v)
    {       Intent parentIntent = getIntent();
        long id = parentIntent.getLongExtra(MainActivity.ACCOUNT_ID,2);
        Intent intent = new Intent(this,AdvanceSearchActivity.class);
        intent.putExtra(ACCOUNT_ID,id);
        startActivity(intent);
    }
    public void addExpert(View v)
    {
        Intent intent = new Intent(this,AddExpertActivity.class);
        startActivity(intent);
    }
    public void createFilm(View v)
    {
        Intent intent = new Intent(this,AddFilmActivity.class);
        startActivity(intent);
    }
    public void loadSystemData(View v)
    {
        List<Film> films = new ArrayList<>();
        Gson gson = new Gson();
      BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            AssetManager assetManager = getAssets();
            InputStream inputStream = assetManager.open("movies.json");
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

            BufferedReader br = new BufferedReader(inputStreamReader);
            //convert the json string back to object
            String buffer;
            while ((buffer = br.readLine()) != null)
            {
                Film tempFilmObj = gson.fromJson(buffer, Film.class);
                films.add(tempFilmObj);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Film f:films)
        {
            Film TempF = new Film(f.getName(),f.getFilmYear(),f.getFilmCountry(),f.getFilmGeners(),f.getFilmDuration(),f.getFilmDirector(),f.getFilmDescription());
            TempF.save();
        }
    }
}
