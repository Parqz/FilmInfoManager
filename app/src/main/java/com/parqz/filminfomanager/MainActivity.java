package com.parqz.filminfomanager;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static final String ACCOUNT_ID = "com.parqz.filminfomanager.MainActivity_ID";
    public static final String EXPERT_ID = "com.parqz.filminfomanager.MainActivity_EXPID";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        //Load system Data
        List<Film> filmList = Film.listAll(Film.class);
        if(filmList.isEmpty())
        {
            List<Film> films = new ArrayList<>();
            Gson gson = new Gson();
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            try {
                AssetManager assetManager = getAssets();
                InputStream inputStream = assetManager.open("movies.json");
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                BufferedReader br = new BufferedReader(inputStreamReader);
                String buffer;
                while ((buffer = br.readLine()) != null)
                {
                    Film tempFilmObj = gson.fromJson(buffer, Film.class);
                    films.add(tempFilmObj);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            for (Film f:films)
            {
                Film TempF = new Film(f.getName(),f.getFilmYear(),f.getFilmCountry(),f.getFilmGeners(),f.getFilmDuration(),f.getFilmDirector(),f.getFilmDescription());
                TempF.save();
            }
        }
        //initialize an Admin Account
        List<Account> accounts = Account.listAll(Account.class);
        if(accounts.isEmpty())
        {
            Account account = new Account("ali","110",25,"admin");
            account.save();

        }
        List<Expert> experts = Expert.listAll(Expert.class);
        if (experts.isEmpty())
        {
            Account account = new Account("meysam","12",25,"expert");
            account.save();
            Expert expert = new Expert("meysam","12",25);
            expert.save();
            account = new Account("parsa","14",25,"expert");
            account.save();
            expert = new Expert("parsa","14",25);
            expert.save();
            account = new Account("sasan","16",25,"expert");
            account.save();
            expert = new Expert("sasan","16",25);
            expert.save();
        }


    }
    public void createAccount(View v)
    {
        Intent intent = new Intent(this,CreateAccountActivity.class);
        startActivity(intent);
    }
    public void login(View v)
    {
        String name,pass;
        String item = "Invalid Name Or Password,Please Enter Correct Name And Password";
        EditText userEditText = (EditText) findViewById(R.id.Name_editText);
        EditText passEditText = (EditText) findViewById(R.id.expert_pass_editText);
        name = userEditText.getText().toString();
        pass = passEditText.getText().toString();
        List<Expert> experts = Select.from(Expert.class).where(Condition.prop("name").eq(name), Condition.prop("pass").eq(pass)).list();
        List<Account> accounts = Select.from(Account.class).where(Condition.prop("name").eq(name), Condition.prop("pass").eq(pass)).list();
        if(accounts.size() == 1) {
            String type = accounts.get(0).getType();
            long id = accounts.get(0).getId();
            Intent intent = null;
            switch (type) {
                case "admin":
                    intent = new Intent(this, AdminDashboardActivity.class);
                    intent.putExtra(ACCOUNT_ID,id);
                    break;
                case "expert":
                    intent = new Intent(this, ExpertDashboardActivity.class);
                    Long expid = experts.get(0).getId();
                    intent.putExtra(EXPERT_ID,expid);
                    intent.putExtra(ACCOUNT_ID,id);
                    break;
                case "users":
                    intent = new Intent(this, UserDashboardActivity.class);
                    intent.putExtra(ACCOUNT_ID,id);
                    break;
            }

            startActivity(intent);
        }
        else
            {
            Toast.makeText(getBaseContext(), item, Toast.LENGTH_LONG).show();
            userEditText.setText("");
            passEditText.setText("");
            }
    }

    public void guestLogin(View v)
    {
        Intent intent = new Intent(this,guestActivity.class);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
