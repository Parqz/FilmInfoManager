package com.parqz.filminfomanager;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class GuestSearchResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_search_result);
        Intent parentIntent = getIntent();
        String name = parentIntent.getStringExtra(guestActivity.SEARCH_NAME);
        List<Film> films = Film.find(Film.class, "name = ?", name);
        List<String> results = new ArrayList<>();
        for (Film f : films) {
            String temp = f.getName() + "-" + f.getFilmCountry() + "-" + f.getFilmYear();
            results.add(temp);
        }

        ArrayAdapter adapter = new ArrayAdapter(this,R.layout.activity_search_listview,results);
        ListView listView = (ListView)findViewById(R.id.guest_search_result_listView);
        listView.setAdapter(adapter);
    }
}