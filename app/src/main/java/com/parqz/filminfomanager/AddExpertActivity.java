package com.parqz.filminfomanager;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

public class AddExpertActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_expert);
    }
    public void createExpert(View v)
    {
        String name,pass,age;
        EditText nameText = (EditText) findViewById(R.id.expert_name_editText);
        EditText passText = (EditText) findViewById(R.id.expert_pass_editText);
        EditText ageText = (EditText) findViewById(R.id.expert_age_editText);
        name = nameText.getText().toString();
        pass = passText.getText().toString();
        age = ageText.getText().toString();
        int a = Integer.parseInt(age);

        Account account = new Account(name,pass,a,"expert");
        account.save();
        Expert expert = new Expert(name,pass,a);
        expert.save();
        Toast.makeText(getBaseContext(), "EXPERT CREATED SUCCESSFULLY!", Toast.LENGTH_LONG).show();

    }
}
