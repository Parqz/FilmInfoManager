package com.parqz.filminfomanager;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class UserDashboardActivity extends AppCompatActivity {
    public static final String ACCOUNT_ID = "com.parqz.filminfomanager.UserDashboardActivity_ID";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_dashboard);
    }
    public void UserSearchFilm(View v)
    {       Intent parentIntent = getIntent();
        long id = parentIntent.getLongExtra(MainActivity.ACCOUNT_ID,2);
        Intent intent = new Intent(this,UserAdvanceSearchActivity.class);
        intent.putExtra(ACCOUNT_ID,id);
        startActivity(intent);
    }
    public void editProfile(View v)
    {       Intent parentIntent = getIntent();
        long id = parentIntent.getLongExtra(MainActivity.ACCOUNT_ID,2);
        Intent intent = new Intent(this,EditProfileActivity.class);
        intent.putExtra(ACCOUNT_ID,id);
        startActivity(intent);
    }
}
