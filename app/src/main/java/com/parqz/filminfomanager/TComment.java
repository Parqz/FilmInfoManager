package com.parqz.filminfomanager;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by PARQZ on 7/28/2016.
 */
@Table
public class TComment extends SugarRecord {
    private Long tcm_id;
    String comment;
    Film film;
    Account account;
    public TComment()
    {}
    public TComment(String cm,Film f,Account ac)
    {
        this.comment = cm;
        this.film = f;
        this.account = ac;
    }
    public Long getTcm_id()
    {
        return tcm_id;
    }
    public String getComment()
    {
        return this.comment;
    }
    public String toString()
    {
        return getComment()+" "+getId()+" "+film+" "+account;
    }

}