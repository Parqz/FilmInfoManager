package com.parqz.filminfomanager;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by PARQZ on 7/26/2016.
 */
@Table
public class Comment extends SugarRecord{
    private Long cm_id;
    String comment;
    Film film;
    Account account;
    public Comment()
    {}
    public Comment(String cm,Film f,Account ac)
    {
        this.comment = cm;
        this.film = f;
        this.account = ac;
    }
    public Long getCm_id()
    {
        return cm_id;
    }
    public String getComment()
    {
        return this.comment;
    }
    public String getAccountName()
    {
        return this.account.getName();
    }
    public String toString()
    {
        return this.comment+" "+film+" "+account;
    }

}
