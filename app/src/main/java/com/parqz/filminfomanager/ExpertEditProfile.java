package com.parqz.filminfomanager;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ExpertEditProfile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expert_edit_profile);
    }
    public void updateExpert(View v)
    {
        Intent parentIntent = getIntent();
        Long acid = parentIntent.getLongExtra(ExpertDashboardActivity.ACCOUNT_ID,1);
        Long exid = parentIntent.getLongExtra(ExpertDashboardActivity.EXPERT_ID,1);
        String name,pass,age;
        EditText nameText = (EditText) findViewById(R.id.expert_edit_name_editText);
        EditText passText = (EditText) findViewById(R.id.expert_edit_pass_editText);
        EditText ageText = (EditText) findViewById(R.id.expert_edit_age_editText);
        name = nameText.getText().toString();
        pass = passText.getText().toString();
        age = ageText.getText().toString();
        int a = Integer.parseInt(age);
        Account account = Account.findById(Account.class,acid);
        account.setName(name);
        account.setPass(pass);
        account.setAge(a);
        account.save();
        Expert expert = Expert.findById(Expert.class,exid);
        expert.setName(name);
        expert.setPass(pass);
        expert.setAge(a);
        expert.save();

    }
}
