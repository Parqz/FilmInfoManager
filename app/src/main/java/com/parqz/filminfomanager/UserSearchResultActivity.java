package com.parqz.filminfomanager;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class UserSearchResultActivity extends AppCompatActivity {
    List<Film> films = new ArrayList<>();
    public static final String ACCOUNT_ID = "com.parqz.filminfomanager.UserSearchResultActivity_ID";
    public static final String NAME_SELECTED = "com.parqz.filminfomanager.UserSearchResultActivity_NAME_SEL";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_search_result);

        Intent parentIntent = getIntent();
        final Long id = parentIntent.getLongExtra(UserAdvanceSearchActivity.ACCOUNT_ID,2);
        String searchType = parentIntent.getStringExtra(UserAdvanceSearchActivity.SEARCH_TYPE);
        String searchEntity = parentIntent.getStringExtra(UserAdvanceSearchActivity.SEARCH_ENTITY);
        Account account = Account.findById(Account.class,id);
        switch (searchType)
        {
            case "Country":
                films = account.searchByCountry(searchEntity);
                break;
            case "Year":
                int year = Integer.getInteger(searchEntity);
                films = account.searchByYear(year);
                break;
            case "Name":
                films = account.searchByName(searchEntity);
                break;
            case "Director":
                films = account.searchByDirector(searchEntity);
        }

        List<String> results = new ArrayList<>();
        for (Film f : films) {
            String temp = f.getName();
            results.add(temp);
        }
        TextView tv = (TextView) findViewById(R.id.user_search_result_header);
        tv.setText("Search By "+searchType+":");
        ArrayAdapter adapter = new ArrayAdapter(this,R.layout.activity_search_listview,results);
        ListView listView = (ListView)findViewById(R.id.user_search_result_listView);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent parentIntent = getIntent();
                Long id = parentIntent.getLongExtra(UserAdvanceSearchActivity.ACCOUNT_ID,2);
                Intent intent = new Intent(UserSearchResultActivity.this,FilmSpecActivity.class);
                String item = ((TextView)view).getText().toString();
                intent.putExtra(ACCOUNT_ID,id);
                intent.putExtra(NAME_SELECTED,item);
                startActivity(intent);
            }
        });

    }
}
