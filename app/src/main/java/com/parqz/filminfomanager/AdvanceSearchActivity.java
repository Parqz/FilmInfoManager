package com.parqz.filminfomanager;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class AdvanceSearchActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{
    public String item;
    public static final String SEARCH_TYPE = "com.parqz.filminfomanager_STYPE";
    public static final String SEARCH_ENTITY = "com.parqz.filminfomanager_SENTITY";
    public static final String ACCOUNT_ID = "com.parqz.filminfomanager.AdvanceSearchActivity_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advance_search);
        //spinner element
        Spinner spinner = (Spinner) findViewById(R.id.search_spinner);
        //spinner click listener
        spinner.setOnItemSelectedListener(this);
        //spinner elements
        List<String> searchTypes = new ArrayList<String>();
        searchTypes.add("Name");
        searchTypes.add("Year");
        searchTypes.add("Country");
        searchTypes.add("Director");
        //Adapter for spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,searchTypes);
        //Drop down layout style
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //// attaching data adapter to spinner
        spinner.setAdapter(adapter);

    }
    public void advSearch(View view)
    {       Intent parentIntent = getIntent();
        long id = parentIntent.getLongExtra(AdminDashboardActivity.ACCOUNT_ID,2);
        Intent intent = new Intent(this,SearchResultActivity.class);
        EditText searchEditText = (EditText)findViewById(R.id.search_field_edittext);
        String searchEntity = searchEditText.getText().toString();
        intent.putExtra(SEARCH_TYPE,item);
        intent.putExtra(SEARCH_ENTITY,searchEntity);
        intent.putExtra(ACCOUNT_ID,id);
        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
        this.item = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
