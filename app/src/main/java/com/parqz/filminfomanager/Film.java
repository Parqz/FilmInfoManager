package com.parqz.filminfomanager;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

import java.util.List;

/**
 * Created by PARQZ on 7/25/2016.
 */
@Table
public class Film extends SugarRecord{
    private Long f_id;
    private String name;
    private int year;
    private String country;
    private List<String> genreList;
    private int durationMinutes;
    private String director;
    private String description;

    //constructor//
    public Film ()
    {}
    public Film (String n,int y,String c,List<String> gl,int dur,String dir,String des )
    {
        this.name = n;
        this.year = y;
        this.country = c;
        this.genreList = gl;
        this.durationMinutes = dur;
        this.director = dir;
        this.description = des;
    }
    public String getName()
    {
        return this.name;
    }
    public int getFilmYear()
    {
        return this.year;
    }
    public int getFilmDuration()
    {
        return this.durationMinutes;
    }
    public String getFilmCountry()
    {
        return this.country;
    }
    public String getFilmDirector()
    {
        return this.director;
    }
    public String getFilmDescription()
    {
        return this.description;
    }
    public List<String> getFilmGeners()
        {
        return this.genreList;
        }
    public Long getF_id()
        {
        return f_id;
        }
        // list all comments of this film
     public List<Comment> getComments()
    {   List<Comment> comments = Comment.find(Comment.class,"film =?",getId().toString() );
        return comments;
    }
    @Override
    public String toString()
    {
        return "Name:" +getName()+ ",Year:" + getFilmYear()+", Duration:"+getFilmDuration()+ ",Country:"+getFilmCountry()+ ",Genere:"+getFilmGeners()+",Director:"+getFilmDirector()+",Description:"+getFilmDescription();
    }
}//end of class Film
