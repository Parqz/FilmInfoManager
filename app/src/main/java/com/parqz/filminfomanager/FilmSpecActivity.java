package com.parqz.filminfomanager;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class FilmSpecActivity extends AppCompatActivity {
    public static final String ACCOUNT_ID = "com.parqz.filminfomanager.FilmSpecActivity_AC_ID";
    public static final String FILM_ID = "com.parqz.filminfomanager.FilmSpecActivity_F_ID";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_spec);
        Intent parentIntent = getIntent();
        Long id = parentIntent.getLongExtra(UserSearchResultActivity.ACCOUNT_ID,1);
        Account account = Account.findById(Account.class,id);
        String FName = parentIntent.getStringExtra(UserSearchResultActivity.NAME_SELECTED);
        List<Film> films = Film.find(Film.class,"name = ?",FName);
        Film film = films.get(0);
        String filmSpec = film.toString();
        TextView spectextview = (TextView) findViewById(R.id.film_spec_textView);
        spectextview.setText(filmSpec);
        List<Comment> comments = film.getComments();
        List<String> cmntResults = new ArrayList<>();
        for (Comment cm:comments) {
            String temp = cm.getAccountName()+" : "+ cm.getComment();
            cmntResults.add(temp);
        }
        ArrayAdapter adapter = new ArrayAdapter(this,R.layout.activity_search_listview,cmntResults);
        ListView listView = (ListView)findViewById(R.id.comments_listview);
        listView.setAdapter(adapter);
    }

    public void addCM(View view)
    {
        Intent parentIntent = getIntent();
        Long acid = parentIntent.getLongExtra(UserSearchResultActivity.ACCOUNT_ID,1);
        String FName = parentIntent.getStringExtra(UserSearchResultActivity.NAME_SELECTED);
        List<Film> films = Film.find(Film.class,"name = ?",FName);
        Long fid = films.get(0).getId();
        Intent intent = new Intent(this,AddCommentActivity.class);
        intent.putExtra(ACCOUNT_ID,acid);
        intent.putExtra(FILM_ID,fid);
        startActivity(intent);
    }
}
