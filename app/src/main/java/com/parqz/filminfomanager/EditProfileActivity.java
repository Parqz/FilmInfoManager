package com.parqz.filminfomanager;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

public class EditProfileActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
    }

    public void updateUser(View v)
    {
        Intent parentIntent = getIntent();
        Long id = parentIntent.getLongExtra(UserDashboardActivity.ACCOUNT_ID,1);
        String name,pass,age;
        EditText nameText = (EditText) findViewById(R.id.edit_name_editText);
        EditText passText = (EditText) findViewById(R.id.edit_pass_editText);
        EditText ageText = (EditText) findViewById(R.id.edit_age_editText);
        name = nameText.getText().toString();
        pass = passText.getText().toString();
        age = ageText.getText().toString();
        int a = Integer.parseInt(age);
        Account account = Account.findById(Account.class,id);
        account.setName(name);
        account.setPass(pass);
        account.setAge(a);
        account.save();

    }
}
