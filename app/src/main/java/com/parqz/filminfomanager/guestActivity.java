package com.parqz.filminfomanager;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class guestActivity extends AppCompatActivity {
public static final String SEARCH_NAME = "com.parqz.filminfomanager.guestActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest);
    }
    public void guestSearch(View view)
    {
        Intent intent = new Intent(this,GuestSearchResultActivity.class);
        EditText et = (EditText) findViewById(R.id.guest_search_edittext);
        String name = et.getText().toString();
        intent.putExtra(SEARCH_NAME,name);
        startActivity(intent);
    }
}
