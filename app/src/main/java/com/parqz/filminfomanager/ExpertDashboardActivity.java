package com.parqz.filminfomanager;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.List;

public class ExpertDashboardActivity extends AppCompatActivity {
    public static final String ACCOUNT_ID = "com.parqz.filminfomanager.ExpertDashboardActivity_ID";
    public static final String EXPERT_ID = "com.parqz.filminfomanager.ExpertDashboardActivity_EXPTID";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expert_dashboard);
    }

    public void viewComments(View view)
    {
        Intent parentIntent = getIntent();
        long acid = parentIntent.getLongExtra(MainActivity.ACCOUNT_ID,2);
        long expid = parentIntent.getLongExtra(MainActivity.EXPERT_ID,2);
        Intent intent = new Intent(this,ViewCommentsActivity.class);
        //Comment.deleteAll(Comment.class);
        //TComment.deleteAll(TComment.class);
        //Cmntexpt.deleteAll(Cmntexpt.class);
        intent.putExtra(ACCOUNT_ID,acid);
        intent.putExtra(EXPERT_ID,expid);
        startActivity(intent);
    }
    public void editExpertProfile(View view)
    {
        Intent intent = new Intent(this,ExpertEditProfile.class);
        Intent parentIntent = getIntent();
        long acid = parentIntent.getLongExtra(MainActivity.ACCOUNT_ID,2);
        long expid = parentIntent.getLongExtra(MainActivity.EXPERT_ID,2);
        intent.putExtra(ACCOUNT_ID,acid);
        intent.putExtra(EXPERT_ID,expid);
        startActivity(intent);

    }
}
