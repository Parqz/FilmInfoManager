package com.parqz.filminfomanager;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by PARQZ on 7/26/2016.
 */
public class Expert extends SugarRecord {
    //private Long ex_id;
    String name;
    String pass;
    int age;
    public Expert()
    {}
    public Expert(String n,String p,int a)
    {
        this.name = n;
        this.pass = p;
        this.age = a;
    }
//    public Long getEx_id()
//        {
//            return ex_id;
//        }
    public String getPass()
    {
        return this.pass;
    }

    public String getName()
    {
        return this.name;
    }
    public int getAge()
    {
        return this.age;
    }
    public void setAge(int a)
    {
        this.age=a;
    }
    public void setName(String name )
    {
        this.name=name;
    }
    public void setPass (String p)
    {
        this.pass=p;
    }
    public String toString()
    {
        return getName()+getId()+getPass()+getAge();
    }
}
