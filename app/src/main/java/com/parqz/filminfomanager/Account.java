package com.parqz.filminfomanager;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

import java.util.List;

/**
 * Created by PARQZ on 7/25/2016.
 */
@Table
public class Account extends SugarRecord{
    private Long ac_id;
    private String name;
    private String pass;
    private int age;
    private String type;

    public Account()
    {}

    /**
     *
     * @param n
     * @param p
     * @param a
     * @param t
     */
    public Account(String n,String p,int a,String t)
    {
        this.age=a;
        this.name=n;
        this.pass=p;
        this.type=t;
    }

    public Long getAc_id()
    {
        return ac_id;
    }
    public String getType()
    {
        return this.type;
    }
    public String getPass()
    {
        return this.pass;
    }

    public String getName()
    {
        return this.name;
    }
    public int getAge()
    {
        return this.age;
    }

    public void setAge(int a)
    {
        this.age=a;
    }
    public void setName(String name )
    {
        this.name=name;
    }
    public void setPass (String p)
    {
        this.pass=p;
    }
    public String getAccountType() {return this.type;}
    public List<Film> searchByName(String name)
    {
        List<Film> films = Film.find(Film.class,"name = ?",name);
        return  films;
    }
    public List<Film> searchByYear(int year)
    {   String y =Integer.toString(year);
        List<Film> films = Film.find(Film.class,"year = ?",y);
        return  films;
    }
    public List<Film> searchByCountry(String country)
    {
        List<Film> films = Film.find(Film.class,"country = ?",country);
        return  films;
    }
    public List<Film> searchByDirector(String director)
    {
        List<Film> films = Film.find(Film.class,"director = ?",director);
        return  films;
    }
    public String toString()
    {
        return getName()+" "+getPass()+" "+getAge()+" "+getId()+" "+getAccountType();
    }

}//End Of Class Account

