package com.parqz.filminfomanager;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

public class CreateAccountActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
    }
    public void createUser(View v)
    {
        String name,pass,age;
        EditText nameText = (EditText) findViewById(R.id.expert_name_editText);
        EditText passText = (EditText) findViewById(R.id.expert_pass_editText);
        EditText ageText = (EditText) findViewById(R.id.expert_age_editText);
        name = nameText.getText().toString();
        pass = passText.getText().toString();
        age = ageText.getText().toString();
        int a = Integer.parseInt(age);
        Account account = new Account(name,pass,a,"users");
        account.save();
        Toast.makeText(this,"Account Created Successfully..."  , Toast.LENGTH_SHORT).show();
        List<Account> accs =Account.listAll(Account.class);
        int i  = accs.size();
    }
}
