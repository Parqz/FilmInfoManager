package com.parqz.filminfomanager;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SearchResultActivity extends AppCompatActivity {
    List<Film> films = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        Intent parentIntent = getIntent();
        Long id = parentIntent.getLongExtra(AdvanceSearchActivity.ACCOUNT_ID,2);
        String searchType = parentIntent.getStringExtra(AdvanceSearchActivity.SEARCH_TYPE);
        String searchEntity = parentIntent.getStringExtra(AdvanceSearchActivity.SEARCH_ENTITY);
        Account account = Account.findById(Account.class,id);
        switch (searchType)
        {
            case "Country":
                films = account.searchByCountry(searchEntity);
                break;
            case "Year":
                int year = Integer.getInteger(searchEntity);
                films = account.searchByYear(year);
                break;
            case "Name":
                films = account.searchByName(searchEntity);
                break;
            case "Director":
                films = account.searchByDirector(searchEntity);
        }

         List<String> results = new ArrayList<>();
        for (Film f : films) {
            String temp = f.getName()+"-"+f.getFilmCountry()+"-"+f.getFilmYear();
            results.add(temp);
        }
        TextView tv = (TextView) findViewById(R.id.search_result_header);
        tv.setText("Search By"+searchType);
        ArrayAdapter adapter = new ArrayAdapter(this,R.layout.activity_search_listview,results);
        ListView listView = (ListView)findViewById(R.id.search_result_listView);
        listView.setAdapter(adapter);
    }
}
