package com.parqz.filminfomanager;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class AddFilmActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_film);
    }
    public void addFilm(View view)
    {
        String name,country,dir,desc,year,dur;
        List<String> genre = new ArrayList<>();
        EditText nameET = (EditText)findViewById(R.id.film_name_editText);
        EditText yearET = (EditText)findViewById(R.id.film_year_editText);
        EditText countryET = (EditText)findViewById(R.id.film_country_editText);
        EditText genreET = (EditText)findViewById(R.id.film_genre_editText);
        EditText durET = (EditText)findViewById(R.id.film_dur_editText);
        EditText dirET = (EditText)findViewById(R.id.film_direct_editText);
        EditText descrptET = (EditText)findViewById(R.id.film_desc_editText);
        name = nameET.getText().toString();
        year = yearET.getText().toString();
        dur = durET.getText().toString();
        int y = Integer.parseInt(year);
        int d = Integer.parseInt(dur);
        country = countryET.getText().toString();
        genre.add(genreET.getText().toString());
        dir = dirET.getText().toString();
        desc = descrptET.getText().toString();
        Film film = new Film(name,y,country,genre,d,dir,desc);
        film.save();
        Toast.makeText(getBaseContext(), "FILM CREATED SUCCESSFULLY!", Toast.LENGTH_LONG).show();
    }
}
