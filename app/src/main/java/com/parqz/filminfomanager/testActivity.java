package com.parqz.filminfomanager;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class testActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        List<Account> accs =Account.listAll(Account.class);
        int i  = accs.size();
        for (Account a:accs
             ) {
            TextView tv = new TextView(this);
            tv.setTextSize(15);
            tv.setText(a.getName()+a.getPass()+a.getAge());
            LinearLayout layout = (LinearLayout) findViewById(R.id.activity_test);
            layout.addView(tv);

        }

    }
}
