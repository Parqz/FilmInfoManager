package com.parqz.filminfomanager;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.Toast;

import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

public class ViewCommentsActivity extends ListActivity {
    ListView listView;
    List<String> res = new ArrayList<>();
    List<Cmntexpt> cmntexpts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_comments);
        Intent parentIntent = getIntent();
        long acid = parentIntent.getLongExtra(ExpertDashboardActivity.ACCOUNT_ID,2);
        long exptid = parentIntent.getLongExtra(ExpertDashboardActivity.EXPERT_ID,2);
        Expert expert = Expert.findById(Expert.class,exptid);
        cmntexpts = Cmntexpt.find(Cmntexpt.class,"expert = ?", expert.getId().toString());
        for (Cmntexpt cmex:cmntexpts)
        {
            String temp = cmex.getComment().toString();
            res.add(temp);
        }
        listView = getListView();
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listView.setTextFilterEnabled(true);
        ArrayAdapter adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_checked,res);
        listView.setAdapter(adapter);
    }
    public void onListItemClick(ListView parent, View v, int position, long id){
        CheckedTextView item = (CheckedTextView) v;
        if(item.isChecked())
            cmntexpts.get(position).allowed = 1;
        else
            cmntexpts.get(position).allowed = 0;
        cmntexpts.get(position).save();
        Toast.makeText(this, cmntexpts.get(position).getComment() +cmntexpts.get(position).allowed+ " checked : " +
                item.isChecked(), Toast.LENGTH_SHORT).show();
    }
    public void applyComments(View view)
    {
        for (String cms:res) {
            List<Cmntexpt> cmxps = Select.from(Cmntexpt.class).where(Condition.prop("comment").eq(cms), Condition.prop("allowed").eq(1)).list();
            if(cmxps.size() >= 2)
            {
                List<TComment> tcms = TComment.find(TComment.class, "comment = ?", cms);
                Comment  cmnt = new Comment(cms,tcms.get(0).film,tcms.get(0).account);
                cmnt.save();
                tcms.get(0).delete();
                List<Cmntexpt> cmexs = Cmntexpt.find(Cmntexpt.class,"comment = ?",cms);
                int i=0;
                while (!cmexs.isEmpty())
                {
                    cmexs.get(i).delete();
                    i++;
                }
            }

        }
    }
}
