package com.parqz.filminfomanager;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by PARQZ on 8/5/2016.
 */
public class Cmntexpt extends SugarRecord {
   // private Long cmtexp_id;
    String comment;
    int allowed;
    Expert expert;
    //constructors
    public Cmntexpt()
    {}
    public Cmntexpt(String cm,int al,Expert ex)
    {
        this.comment = cm;
        this.allowed = al;
        this.expert = ex;
    }
    public String getComment()
    {
        return this.comment;
    }
    public String toString()
    {
        return getComment()+" "+getId()+" "+allowed+" "+expert;
    }



}
